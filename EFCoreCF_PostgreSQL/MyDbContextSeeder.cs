﻿using System;
namespace EFCoreCF_PostgreSQL
{
    public static class MyDbContextSeeder
    {

        public static void Seed(MyDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            context.Products.Add(new Product()
            {
                ProductName = "ProductName",
                InStock = 1,
                Price = 40.20
            });
            context.SaveChanges();
        }
    }

}
