﻿using Microsoft.EntityFrameworkCore;

namespace EFCoreCF_PostgreSQL
{
public class MyDbContext : DbContext
{

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {

            optionsBuilder.UseNpgsql(@"host=127.0.0.1;database=test;user id=postgres;");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

//        modelBuilder.Entity<OrderDetail>()
//          .HasKey(p => new { p.OrderID, p.ProductID });
    }

    public DbSet<Product> Products { get; set; }
    }
}
