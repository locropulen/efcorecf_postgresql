﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCoreCF_PostgreSQL
{
    [Table("Product")]
        public class Product
        {

            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long ProductID { get; set; }

            [Required]
            [MaxLength(50)]
            public string ProductName { get; set; }

            public long InStock { get; set; }
            public double Price { get; set; }
        
        }
    }