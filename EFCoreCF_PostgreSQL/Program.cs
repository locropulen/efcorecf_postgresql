﻿using System;
using Microsoft.EntityFrameworkCore;

namespace EFCoreCF_PostgreSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new MyDbContext();

            Console.WriteLine("Entity Framework Core Code-First sample");
            Console.WriteLine();

            MyDbContextSeeder.Seed(context);

            Console.WriteLine("Products Seed");
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
